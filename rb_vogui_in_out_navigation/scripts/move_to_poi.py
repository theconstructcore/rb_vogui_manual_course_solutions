#!/usr/bin/env python

import rospy
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseResult


class MoveVoguiBase(object):

    def __init__(self):

        self.client = actionlib.SimpleActionClient('/robot/move_base', MoveBaseAction)
    
        # We define each POI as a move Base goal
        ## HOME ##
        home_poi = MoveBaseGoal()
        home_poi.target_pose.header.frame_id = "robot_map"
        home_poi.target_pose.header.stamp = rospy.Time.now()
        home_poi.target_pose.pose.position.x = -1.97296142578
        home_poi.target_pose.pose.position.y = -5.05079269409
        home_poi.target_pose.pose.position.z = 0.0
        home_poi.target_pose.pose.orientation.x = 0.0
        home_poi.target_pose.pose.orientation.y = 0.0
        home_poi.target_pose.pose.orientation.z = -0.0224237328117
        home_poi.target_pose.pose.orientation.w = 0.999748556491

        ## PLACE ##
        place_poi = MoveBaseGoal()
        place_poi.target_pose.header.frame_id = "robot_map"
        place_poi.target_pose.header.stamp = rospy.Time.now()
        place_poi.target_pose.pose.position.x = -1.26938509941
        place_poi.target_pose.pose.position.y = -9.359333992
        place_poi.target_pose.pose.position.z = 0.0
        place_poi.target_pose.pose.orientation.x = 0.0
        place_poi.target_pose.pose.orientation.y = 0.0
        place_poi.target_pose.pose.orientation.z = 0.997106367044
        place_poi.target_pose.pose.orientation.w = 0.0760190292049


        ## IN OUT TRANSITION ZONE ##
        in_out_transition_zone_poi = MoveBaseGoal()
        in_out_transition_zone_poi.target_pose.header.frame_id = "robot_map"
        in_out_transition_zone_poi.target_pose.header.stamp = rospy.Time.now()
        in_out_transition_zone_poi.target_pose.pose.position.x = -0.0655038356781
        in_out_transition_zone_poi.target_pose.pose.position.y = 1.22955369949
        in_out_transition_zone_poi.target_pose.pose.position.z = 0.0
        in_out_transition_zone_poi.target_pose.pose.orientation.x = 0.0
        in_out_transition_zone_poi.target_pose.pose.orientation.y = 0.0
        in_out_transition_zone_poi.target_pose.pose.orientation.z = 0.722175171601
        in_out_transition_zone_poi.target_pose.pose.orientation.w = 0.691710214991


        self.keys_list = ["home", "place", "in_out_transition_zone"]
        self.values_list = [home_poi, place_poi, in_out_transition_zone_poi]
        zip_iterator = zip(self.keys_list, self.values_list)
        self.poi_dict = dict(zip_iterator)

        rospy.loginfo("Waiting move base action...")
        self.client.wait_for_server()
        rospy.loginfo("move base action ready!")


    def start_ask_loop(self):

        selection = "START"

        while selection != "Q":
            rospy.loginfo("#############################")
            rospy.loginfo("Select Point of Interest")
            rospy.loginfo("[0]HOME POI")
            rospy.loginfo("[1]PLACE POI")
            rospy.loginfo("[2]IN OUT TRANSITION ZONE POI")
            rospy.loginfo("[Q]END program")
            selection = input(">>")
            self.select_action(selection)

    def select_action(self,selection):
        try:
            int_selection = int(selection)
            if int_selection < len(self.keys_list):
                poi_name = self.keys_list[int_selection]
                self.move_to_poi(poi_name)
            else:
                rospy.logerr("Not valid int="+str(int_selection))
        except:
            rospy.logerr("Not an int")
    
    def move_to_poi(self,name_poi):

        rospy.logwarn("Selected POI ="+str(name_poi))

        if name_poi in self.poi_dict:
            goal_poi = self.poi_dict[name_poi]
            rospy.loginfo("Sending Goal for poi "+str(name_poi))
            self.client.send_goal(goal_poi)
            rospy.loginfo("Goal for poi "+str(name_poi)+"...SENT")
            result = self.client.wait_for_result()
            rospy.loginfo("Goal successfull! result="+str(result))

        else:
            rospy.logerr("Point of INtereset "+str()+" is not in the Database.")


if __name__ == '__main__':

    rospy.init_node('move_base_test_client')
    mv_obj = MoveVoguiBase()
    mv_obj.start_ask_loop()
    