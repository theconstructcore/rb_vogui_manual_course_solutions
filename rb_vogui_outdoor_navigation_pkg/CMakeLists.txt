cmake_minimum_required(VERSION 3.0.2)
project(rb_vogui_outdoor_navigation_pkg)

find_package(catkin REQUIRED COMPONENTS
  amcl
  gmapping
  map_server
  move_base
  roslaunch
  urdf
  xacro
)

catkin_package(
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)

if (CATKIN_ENABLE_TESTING)
  find_package(roslaunch REQUIRED)
  roslaunch_add_file_check(launch/include/amcl.launch)
  roslaunch_add_file_check(launch/amcl_demo.launch)
  roslaunch_add_file_check(launch/include/gmapping.launch)
  roslaunch_add_file_check(launch/gmapping_demo.launch)
  roslaunch_add_file_check(launch/include/move_base.launch)
  roslaunch_add_file_check(launch/odom_navigation_demo.launch)
endif()

install(
DIRECTORY launch maps params params_vogui
DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)
