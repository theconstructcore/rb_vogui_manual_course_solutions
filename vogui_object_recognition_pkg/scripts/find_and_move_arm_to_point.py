#!/usr/bin/env python

import sys
import rospy
from math import pi
import moveit_commander
import geometry_msgs.msg
from object_pose_extractor import GameGirlObjectDetector

if __name__ == '__main__':

    rospy.init_node('move_arm_to_point_node')

    moveit_commander.roscpp_initialize(sys.argv)

    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()
    # rbvogui_xl_lift_ur10 -> ur10e_arm   rbvogui_xl_bi_arm -> left_arm / right_arm
    group = moveit_commander.MoveGroupCommander("arm")
    group.set_planner_id("RRTstar")
    group.set_planning_time(5)
    group.set_max_velocity_scaling_factor(0.5)

    # We set up now the group for the gripper
    group_gripper = moveit_commander.MoveGroupCommander("gripper")

    # planning_frame = group_gripper.get_planning_frame()
    # print(planning_frame)

    joint_goal_gripper = group_gripper.get_current_joint_values()

    
    # planning_frame = group.get_planning_frame()
    # print(planning_frame)

    # print(robot.get_current_state())

    # Close Gripper Pose
    joint_goal_gripper[0] = 0.32

    print("Sending OPEN goal...")
    group_gripper.go(joint_goal_gripper, wait=True)
    print("Goal OPEN successfull!")



    pose_goal = geometry_msgs.msg.Pose()

    z_displacement = 0.168536864221 / 2.2
    z_displacement_aproach = 0.168536864221
    z_gripper_compensation = 0.2

    # We look for the GameGirl Position
    gamegirl_detected_dict = GameGirlObjectDetector().find_gamegirl()

    gg_x = 0.0
    gg_y = 0.0
    gg_z = 0.0

    for gg_object_pose in gamegirl_detected_dict.values():
        print(gg_object_pose)
        gg_x = gg_object_pose.position.x
        gg_y = gg_object_pose.position.y
        gg_z = gg_object_pose.position.z

    pose_goal.position.x = gg_x
    pose_goal.position.y = gg_y
    pose_goal.position.z = gg_z  + z_displacement_aproach + z_gripper_compensation
    pose_goal.orientation.x = -0.715880945775
    pose_goal.orientation.y = 0.698136877777
    pose_goal.orientation.z = -0.010834657831
    pose_goal.orientation.w = 0.00140767633714

    rospy.loginfo(str(pose_goal))

    group.set_pose_target(pose_goal)

    print("Sending goal To END EFFECTOR APROACH GAMEGIRL...")
    group.go(wait=True)
    print("Goal To END EFFECTOR APROACH GAMEGIRL successfull!")

    

    pose_goal.position.z = gg_z  + z_displacement + z_gripper_compensation

    rospy.loginfo(str(pose_goal))

    group.set_pose_target(pose_goal)

    print("Sending goal To END EFFECTOR GAMEGIRL...")
    group.go(wait=True)
    print("Goal To END EFFECTOR GAMEGIRL successfull!")


    # Close Gripper Pose
    joint_goal_gripper[0] = -0.48

    print("Sending CLOSE goal...")
    group_gripper.go(joint_goal_gripper, wait=True)
    print("Goal CLOSE successfull!")


    pose_goal.position.z = gg_z  + z_displacement_aproach + z_gripper_compensation

    rospy.loginfo(str(pose_goal))

    group.set_pose_target(pose_goal)

    print("Sending goal To UP EFFECTOR GAMEGIRL...")
    group.go(wait=True)
    print("Goal To UP EFFECTOR GAMEGIRL successfull!")


    group.stop()
    group.clear_pose_targets()

    group_gripper.stop()
    group_gripper.clear_pose_targets()


