#!/usr/bin/env python

import sys
import rospy
import moveit_commander
from math import pi

if __name__ == '__main__':

  rospy.init_node('move_arm_joint_by_joint_node')
  
  moveit_commander.roscpp_initialize(sys.argv)

  robot = moveit_commander.RobotCommander()
  scene = moveit_commander.PlanningSceneInterface()
  group = moveit_commander.MoveGroupCommander("arm")

  planning_frame = group.get_planning_frame()
  print(planning_frame)

  print(robot.get_current_state())

  joint_goal = group.get_current_joint_values()

  # Move to perception pose
  joint_goal[0] = 0.207857953717
  joint_goal[1] = -1.60250269661
  joint_goal[2] = 1.99778886435
  joint_goal[3] = -2.6344812586
  joint_goal[4] = -1.59787169356
  joint_goal[5] = 1.8066613393

  print("Sending Percpetion goal...")
  group.go(joint_goal, wait=True)
  print("Goal Perception successfull!")

#   # Move to PreGrasp Pose
#   joint_goal[0] = -0.00607866100589
#   joint_goal[1] = -0.721292272469
#   joint_goal[2] = 0.976957320182
#   joint_goal[3] = -1.84459817148
#   joint_goal[4] = -1.59952197012
#   joint_goal[5] = -0.0307090671113

#   print("Sending Pregrasp goal...")
#   group.go(joint_goal, wait=True)
#   print("Goal Pregrasp successfull!")

#   # Move to Grasp Pose
#   joint_goal[0] = -0.00690772198793
#   joint_goal[1] = -0.673864172353
#   joint_goal[2] = 1.03265146272
#   joint_goal[3] = -1.94754899105
#   joint_goal[4] = -1.59908557767
#   joint_goal[5] = -0.0328483571747

#   print("Sending GRASP goal...")
#   group.go(joint_goal, wait=True)
#   print("Goal GRASP successfull!")

  group.stop()



