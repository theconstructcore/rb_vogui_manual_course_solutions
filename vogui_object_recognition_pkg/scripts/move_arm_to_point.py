#!/usr/bin/env python

import sys
import rospy
from math import pi
import moveit_commander
import geometry_msgs.msg

if __name__ == '__main__':

  rospy.init_node('move_arm_to_point_node')
  
  moveit_commander.roscpp_initialize(sys.argv)

  robot = moveit_commander.RobotCommander()
  scene = moveit_commander.PlanningSceneInterface()
  # rbvogui_xl_lift_ur10 -> ur10e_arm   rbvogui_xl_bi_arm -> left_arm / right_arm
  group = moveit_commander.MoveGroupCommander("arm")
  group.set_planner_id("RRTstar")
  group.set_planning_time(5)
  group.set_max_velocity_scaling_factor(0.5)

  planning_frame = group.get_planning_frame()
  print(planning_frame)

  print(robot.get_current_state())

  pose_goal = geometry_msgs.msg.Pose()
  
  pose_goal.position.x = 0.441629676189
  pose_goal.position.y = 0.204436365985
  pose_goal.position.z = 0.913456865721
  pose_goal.orientation.x = 0.0203760649848
  pose_goal.orientation.y = 0.944504950546
  pose_goal.orientation.z = 0.0643331723579
  pose_goal.orientation.w = 0.321490991016

  group.set_pose_target(pose_goal)

  print("Sending goal To END EFFECTOR Perception...")
  group.go(wait=True)
  print("Goal To END EFFECTOR Perception successfull!")

#   pose_goal.position.x = 0.79601802681
#   pose_goal.position.y = 0.106400423498
#   pose_goal.position.z = 0.74671008484
#   pose_goal.orientation.x = -0.71591622003
#   pose_goal.orientation.y = 0.698098863565
#   pose_goal.orientation.z = -0.0109481438622
#   pose_goal.orientation.w = 0.00144247447105

#   group.set_pose_target(pose_goal)

#   print("Sending goal To END EFFECTOR Pregrasp...")
#   group.go(wait=True)
#   print("Goal To END EFFECTOR Pregrasp successfull!")

#   pose_goal.position.x = 0.796974551147
#   pose_goal.position.y = 0.105667000776
#   pose_goal.position.z = 0.692638455278
#   pose_goal.orientation.x = -0.715880945775
#   pose_goal.orientation.y = 0.698136877777
#   pose_goal.orientation.z = -0.010834657831
#   pose_goal.orientation.w = 0.00140767633714

#   group.set_pose_target(pose_goal)

#   print("Sending goal To END EFFECTOR GRASP...")
#   group.go(wait=True)
#   print("Goal To END EFFECTOR GRASP successfull!")

  group.stop()
  group.clear_pose_targets()


