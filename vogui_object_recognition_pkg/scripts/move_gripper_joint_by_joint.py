#!/usr/bin/env python

import sys
import rospy
import moveit_commander
from math import pi

if __name__ == '__main__':

  rospy.init_node('move_gripper_joint_by_joint_node')
  
  moveit_commander.roscpp_initialize(sys.argv)

  robot = moveit_commander.RobotCommander()
  scene = moveit_commander.PlanningSceneInterface()
  group = moveit_commander.MoveGroupCommander("gripper")

  planning_frame = group.get_planning_frame()
  print(planning_frame)

  print(robot.get_current_state())

  joint_goal = group.get_current_joint_values()

  # Move to perception pose
  joint_goal[0] = -0.50

  print("Sending CLOSE goal...")
  group.go(joint_goal, wait=True)
  print("Goal CLOSE successfull!")

  # Move to perception pose
#   joint_goal[0] = 0.32

#   print("Sending Open goal...")
#   group.go(joint_goal, wait=True)
#   print("Goal Open successfull!")


  group.stop()



