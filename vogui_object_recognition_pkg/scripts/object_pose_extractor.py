#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import numpy

from visualization_msgs.msg import Marker

class GameGirlObjectDetector:

    def __init__(self, table_height_init=0.296816984424, error_height=0.2, dim_x=0.0381507277489,dim_y=0.100709192455,dim_z=0.168536864221, error_dim=0.1):

        self._rate = rospy.Rate(5)

        self.table_height = table_height_init
        self._error_height = error_height
        self.error_dim = error_dim
        self.dim_x = dim_x
        self.dim_y = dim_y
        self.dim_z = dim_z
        self.surface_dict = {}
        self.surface_topic = "/surface_objects"
        self._check_surface_ready()
        rospy.Subscriber(self.surface_topic, Marker, self.surface_callback)

        rospy.loginfo('Ready to detect Surfaces!')

    def _check_surface_ready(self):
        self._surface_data = None
        while self._surface_data is None and not rospy.is_shutdown():
            try:
                self._surface_data = rospy.wait_for_message(self.surface_topic, Marker, timeout=1.0)
                rospy.logdebug("Current "+self.surface_topic+" READY=>" + str(self._surface_data))

            except:
                rospy.logerr("Current "+self.surface_topic+" not ready yet, retrying.")

    def update_table_height(self,new_table_height):
        self.table_height = new_table_height

    def look_for_gamegirl(self, in_dim_x, in_dim_y, in_dim_z, z_height, colour_green, colour_alfa):
        """
        Theer are several things that make the detection of the game girl
        1) It has to be an object, therefore the marker has to be green and semy transparent
        colour_alfa = 0.5
        colour_green = 1.0
        2) The dimensions have to be around these values
        dim_x: 0.0381507277489
        dim_y: 0.100709192455
        dim_z: 0.168536864221
        3) We are looking for the gamegirl at a certain height
        z_height: 0.296816984424
        """
        is_gamegirl = False

        delta_min = z_height - self._error_height
        delta_max = z_height + self._error_height
        is_correct_height = delta_min < self.table_height < delta_max

        if is_correct_height:
            # We check now the colour of marker
            is_object = (colour_green == 1.0) and (colour_alfa == 0.5)
            if is_object:
                # We check the dimensions
                delta_x_min = in_dim_x - self.error_dim
                delta_x_max = in_dim_x + self.error_dim
                delta_y_min = in_dim_y - self.error_dim
                delta_y_max = in_dim_y + self.error_dim
                delta_z_min = in_dim_z - self.error_dim
                delta_z_max = in_dim_z + self.error_dim

                is_dim_x_ok = delta_x_min < self.dim_x < delta_x_max
                is_dim_y_ok = delta_y_min < self.dim_y < delta_y_max
                is_dim_z_ok = delta_z_min < self.dim_z < delta_z_max

                is_dim_ok = is_dim_x_ok and is_dim_y_ok and is_dim_z_ok
                if is_dim_ok:
                    is_gamegirl = True
                else:
                    error_msgs = "Wrong dimensions x="+str(is_dim_x_ok)+",y="+str(is_dim_y_ok)+",z="+str(is_dim_z_ok)
            else:
                error_msgs = "Wrong colour g="+str(colour_green)+",a="+str(colour_alfa)
        else:
            error_msgs = "Wrong Height "+str(z_height)

        return is_gamegirl

    def surface_callback(self, msg):
        """
        uint8 ARROW=0
        uint8 CUBE=1
        uint8 SPHERE=2
        uint8 CYLINDER=3
        uint8 LINE_STRIP=4
        uint8 LINE_LIST=5
        uint8 CUBE_LIST=6
        uint8 SPHERE_LIST=7
        uint8 POINTS=8
        uint8 TEXT_VIEW_FACING=9
        uint8 MESH_RESOURCE=10
        uint8 TRIANGLE_LIST=11
        uint8 ADD=0
        uint8 MODIFY=0
        uint8 DELETE=2
        uint8 DELETEALL=3
        std_msgs/Header header
        uint32 seq
        time stamp
        string frame_id
        string ns
        int32 id
        int32 type
        int32 action
        geometry_msgs/Pose pose
        geometry_msgs/Point position
            float64 x
            float64 y
            float64 z
        geometry_msgs/Quaternion orientation
            float64 x
            float64 y
            float64 z
            float64 w
        geometry_msgs/Vector3 scale
        float64 x
        float64 y
        float64 z
        std_msgs/ColorRGBA color
        float32 r
        float32 g
        float32 b
        float32 a
        duration lifetime
        bool frame_locked
        geometry_msgs/Point[] points
        float64 x
        float64 y
        float64 z
        std_msgs/ColorRGBA[] colors
        float32 r
        float32 g
        float32 b
        float32 a
        string text
        string mesh_resource
        bool mesh_use_embedded_materials
        """


        name = msg.ns
        surface_pose = msg.pose


        if "surface_" in name and not "_axes" in name:
            # We check the heigh in z to see if its the table
            found_gamegirl = self.look_for_gamegirl(in_dim_x=msg.scale.x, 
                                                in_dim_y=msg.scale.y,
                                                in_dim_z=msg.scale.z,
                                                z_height=msg.pose.position.z,
                                                colour_green=msg.color.g,
                                                colour_alfa=msg.color.a)

            if found_gamegirl:
                self.surface_dict[name] = surface_pose
                rospy.loginfo("Found GameGirl")
            else:
                rospy.logwarn("Object NOT GameGirl")
                
        else:
            rospy.logdebug("Surface Object Not found "+str(name))


    def get_gamegirl_detected(self):
        return self.surface_dict

    def run(self):

        while not rospy.is_shutdown():
            gamegirl_detected = self.get_gamegirl_detected()
            rospy.loginfo(str(gamegirl_detected))
            self._rate.sleep()

    def dict_empty(self, in_dict):
        if in_dict:
            return True
        else:
            return False

    def find_gamegirl(self):

        gamegirl_detected = self.get_gamegirl_detected()

        while not self.dict_empty(gamegirl_detected):
            gamegirl_detected = self.get_gamegirl_detected()
            rospy.loginfo(str(gamegirl_detected))
            self._rate.sleep()
        
        rospy.loginfo("GameGirl FOUND")

        return gamegirl_detected



if __name__ == '__main__':
   rospy.init_node('surface_data_extract_node', log_level=rospy.INFO)

   try:
      GameGirlObjectDetector().run()
   except KeyboardInterrupt:
      rospy.loginfo('Shutting down')

